clear all; close all; clc;

data = importdata('AIdata12.dat');
result = data;

% populacja 100, mutujemy, dostajemy 600, wybieramy 100 najlepszych za pomoca MSE, mutujemy...

numberOfParents = 100
numberOfColumns = 6
N = size(data, 1); % data size

numberOfMutants = 5*numberOfParents;

tau1 = 1/sqrt(2*numberOfColumns);
tau2 = 1/(sqrt(2*sqrt(numberOfColumns)));

a = -10;
b = 10;

parentsMatrix = zeros(numberOfParents, numberOfColumns);
offspringMatrix = zeros(numberOfMutants, numberOfColumns);

A = B = C = 0;
sigA = sigB = sigC = 0;

for i = 1 : numberOfParents
    for j = 1 : (numberOfColumns/2)
        parentsMatrix(i,j) = (b-a).*rand + a;
    end
end

sig = 0 +b.*rand(parentsNumber, 3);