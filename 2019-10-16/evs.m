function evs()
tic
populationNumber=100; %mi
how_much_iterations = 100;
number_of_elements = 4;
n =3;
fitBest = 0;
fitBestchild=0;
data = importdata('AIdata10.dat');
data2 = data;
N = size(data,1);
%finding tau1,2
tau1 = 1/(sqrt(2*n));
tau2 = 1/(sqrt(2*sqrt(n)));
tempMatrix = zeros(2*populationNumber,number_of_elements);
chromosomMatrix = zeros(populationNumber,number_of_elements);
childMatrix = zeros(populationNumber,number_of_elements);
r = zeros(populationNumber,3);

a=-100;
b=100;
for d = 1 : populationNumber
chromosomMatrix(d,1) = (b-a).*rand +a;
chromosomMatrix(d,2) = (b-a).*rand +a;
chromosomMatrix(d,3) = (b-a).*rand +a;
end
a=0;
b=10;
sig = 0 + b.*rand(populationNumber, 3);

for ii = 1 : populationNumber
sum = 0;
for k = 1:N
sum = sum + (data(k,2) - (chromosomMatrix(ii,1)*((data(k,1)^2)-chromosomMatrix(ii,2)*cos(chromosomMatrix(ii,3)*pi*data(k,1)))))^2;
end
chromosomMatrix(ii,4) = sum/N;
end

iterations = 0;
condition = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while (condition)
chromosomMatrix = sortrows(chromosomMatrix,4);
fitBest = chromosomMatrix(1,4);
r2 = zeros(1,3);
%finding r
r1 = randn * tau1;
for d=1:3
r2(1,d) = randn * tau2;
end

for k = 1 : populationNumber
r(k,1) = randn * sig(k,1);
r(k,2) = randn * sig(k,2);
r(k,3) = randn * sig(k,3);
end
for j = 1:populationNumber
sig(j,1) = sig(j,1) * exp(r1) * exp(r2(1,1));
sig(j,2) = sig(j,2) * exp(r1) * exp(r2(1,2));
sig(j,3) = sig(j,3) * exp(r1) * exp(r2(1,3));
end
%creating childMatrix
for j = 1 : populationNumber
childMatrix(j,1) = chromosomMatrix(j,1) + r(j,1);
childMatrix(j,2) = chromosomMatrix(j,2) + r(j,2);
childMatrix(j,3) = chromosomMatrix(j,3) + r(j,3);
end
for ii = 1 : populationNumber
sum = 0;
for k = 1 : N
sum = sum+ ((data(k,2) - childMatrix(ii,1)*((data(k,1)^2)-childMatrix(ii,2)*cos(childMatrix(ii,3)*pi*data(k,1)))))^2;
end
childMatrix(ii,4) = sum/N;
end
childMatrix = sortrows(childMatrix,4);
fitBestchild = childMatrix(1,4);
tempMatrix = [chromosomMatrix;childMatrix];
tempMatrix = sortrows(tempMatrix,4);
chromosomMatrix = tempMatrix(1:populationNumber,:);
iterations = iterations + 1;
condition = (iterations ~= how_much_iterations);
% condition = (abs(fitBestchild-fitBest) &gt; 0.1); % epsilon
Difference =abs(fitBestchild-fitBest);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1 : N
data2(k,2) = (chromosomMatrix(1,1)*((data2(k,1)^2)-chromosomMatrix(1,2)*cos(chromosomMatrix(1,3)*pi*data2(k,1))));
end
hold off
plot(data(:,1),data(:,2), 'y');

hold 
plot(data(:,1),data2(:,2), 'r');

A=chromosomMatrix(1,1);
B = chromosomMatrix(1,2);
C = chromosomMatrix(1,3);
A
B
C
TimeSpent = toc;
TimeSpent
iterations
Difference
end