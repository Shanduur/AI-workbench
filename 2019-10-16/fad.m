function ret = fad(a, b, c, x)
    ret = a*(x^2 - b * cos(c*pi()*x));
end