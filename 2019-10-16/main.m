clear all; close all; clc;

data = importdata('AIdata12.dat');
d= data;
n=3;
l=100;

P=zeros(100,4);
O=zeros(100,4);
S=zeros(100,3);
r=zeros(100,3);



for c = 1:3
    for mm = 1:100
        P(mm,c) = randi([-10 10],1,1);
    end
end

for c = 1:3
    for mm = 1:100
        S(mm,c) = randi([0 10],1,1);
    end
end



for c= 1:100
    
    y=0;
    for cc=1:100
    co=(P(c,3))*(3.14*d(cc,1));
    y=y+ (d(cc,2)-(P(c,1)*(d(cc,1)*d(cc,1)-P(c,2)*cos(co))))^2;
    end
    P(c,4)=y/101;
end

t1=1/sqrt(2*n);
t2=1/sqrt(2*sqrt(n));

it=0;
eps=0.001;
cond=true;

while(it~=1000)
  P=sortrows(P,4);
  best1=P(1,4);
  
  r1=randn*t1;
  r2=zeros(1,3);
  
  r2(1,1)=randn*t2;
  r2(1,2)=randn*t2;
  r2(1,3)=randn*t2;
  
  
  
  for i=1:100
    for j=1:3
      r(i,j)=randn*S(i,j);
      S(i,j)=S(i,j)*exp(r1)*exp(r2(1,j));
      O(i,j)=P(i,j)+r(i,j);
      
    end
  end
  
  
  for c= 1:100
      y=0;
    for cc=1:100
        co=(O(c,3))*(3.14*d(cc,1));
        y=y + (d(cc,2)-(O(c,1)*(d(cc,1)*d(cc,1)-O(c,2)*cos(co))))^2;
    end
    
    O(c,4)=y/101;
  end
  
  O=sortrows(O,4);
  best2=O(1,4);
  
  PO=[P;O];
  PO=sortrows(PO,4);
  P=PO(1:100,:);
  it=it+1;
  
  cond =abs(best2-best1)>eps;
end

for i=1:100
    co=(P(1,3))*(3.14*d(i,1));
    d(i,2)=P(1,1)*(d(i,1)*d(i,1)-P(1,2)*cos(co));
    
end

hold off
plot(data(:,1),data(:,2), 'g');

hold 
plot(data(:,1),d(:,2), 'r');

a=P(1,1);
b=P(1,2);
c=P(1,3);

a
b
c