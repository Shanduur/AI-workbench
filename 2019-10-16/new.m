clear all; close all; clc;
data = importdata('AIdata12.dat');
result = data;

parentsNumber = 100;
numberOfColumns = 4;
n = 3;

parentsMatrix = zeros(parentsNumber, numberOfColumns);
offspringMatrix = zeros(parentsNumber, numberOfColumns);
r = zeros(parentsNumber, 3);

a=-10;
b=10;

N = size(data,1);

tau1 = 1/sqrt(2*n);

tau2 = 1/(sqrt(2*sqrt(n)));

for i = 1 : parentsNumber
    for j = 1 : n
        parentsMatrix(i,j) = (b-a).*rand + a;
    end
end

sig = 0 +b.*rand(parentsNumber, 3);

for ii = 1 : parentsNumber
    sum = 0;
    for k = 1:N
        sum = sum + (data(k,2) - (parentsMatrix(ii, 1)*((data(k,1)^2) - parentsMatrix(ii,2)*cos(parentsMatrix(ii,3)*pi*data(k,1)))))^2;
    end
    parentsMatrix(ii,numberOfColumns) = sum/N;
end


iterations =0;
cond = true;
eps= 0.000001;

while (iterations ~= 100)
    parentsMatrix = sortrows(parentsMatrix,4);
    fitBest = parentsMatrix(1,4);
    
    r1= randn * tau1;
    r2= size(1,3);

    for j=1:n
        r2(1,j) = randn * tau2;
    end

    for k=1 :parentsNumber
        for j = 1 : n
            r(k,j) = randn * sig (k,j);
        end
    end

    for k = 1 : parentsNumber
        for i = 1 : n
            sig(k,i) = sig(k,i) * exp (r1) * exp (r2(1,i));
        end
    end

    for j = 1 : parentsNumber
        for k = 1 : n 
            offspringMatrix(j,k) = parentsMatrix(j,k) + r(j,k);
        end
    end

    for i = 1 : parentsNumber
        sum = 0;
        for k = 1 : N
            sum = sum + ((data(k,2) - offspringMatrix(i,1)*((data(k,1)^2) - offspringMatrix(i,2)*cos(offspringMatrix(i,3)*pi*data(k,1)))))^2;
        end
            offspringMatrix(i, numberOfColumns) = sum/N;
    end

    offspringMatrix = sortrows(offspringMatrix, 4);
    fitBestOffspring = offspringMatrix(1,4);
    tempM = [parentsMatrix; offspringMatrix];
    tempM = sortrows(tempM, 4);
    parentsMatrix = tempM(1:parentsNumber,:);
    iterations = iterations + 1;

    cond = abs(fitBestOffspring - fitBest) > eps;
end 

for k = 1 : N
    result(k,2) = (parentsMatrix(1,1)*((result(k,1)^2)-parentsMatrix(1,2)*cos(parentsMatrix(1,3)*pi*result(k,1))));
end

hold off
plot(data(:,1), data(:,2), '-r');

hold
plot(data(:,1), result(:,2), '-g');

A = parentsMatrix(1,1);
B = parentsMatrix(1,2);
C = parentsMatrix(1,3);

A
B
C
iterations